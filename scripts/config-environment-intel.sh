### Steps to config the Intel environment

# Update the system
apt update
apt upgrade -y

# Install GCC-9
# add-apt-repository ppa:ubuntu-toolchain-r/test
# apt update
# apt install gcc-9 -y

# Install necessary packages
apt install build-essential git numactl python3-pip -y

# Install Parallel Studio
apt install alsa xorg g++-multilib linux-headers-4.15.0-50-generic -y
wget http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/15810/parallel_studio_xe_2019_update5_professional_edition.tgz
tar -xzvf parallel_studio_xe_2019_update5_professional_edition.tgz
cd parallel_studio_xe_2019_update5_professional_edition/
./install.sh
cd ..
source /opt/intel/compilers_and_libraries_2019.5.281/linux/bin/compilervars.sh -arch intel64 -platform linux
echo "source /opt/intel/compilers_and_libraries_2019.5.281/linux/bin/compilervars.sh -arch intel64 -platform linux" >> ~/.bash_profile

# Install Devito
git clone https://github.com/opesci/devito.git
cd devito
pip3 install -e .
pip3 install git+https://github.com/opesci/opescibench
pip3 install matplotlib
pip3 install fastcache
cd ..

