#! /usr/bin/bash
#set -x

# set OpenMP pinning
source devito/scripts/set_omp_pinning.sh skx

# machine name - intel5120 or intel6126 or arm_thunderx2
machine=intel5120

# pinning tool
nc="numactl --cpunodebind=1 --membind=1"

# number of runs
nruns=3

# number of cores of the processor
if [[ $machine == arm_thunderx2 ]]; then
   cores=32
else
   if [[ $machine == intel5120 ]]; then
       cores=14
   else
       cores=12
   fi
fi

# available compilers and platform
if [[ $machine == arm_thunderx2 ]]; then
    plat="DEVITO_PLATFORM=arm"
    compilers="gcc cce"
else
    plat="DEVITO_PLATFORM=skx"
    compilers="gcc icc"
fi

# for each compiler
for compiler in $compilers; do

   # for each backend
   for be in core; do

       # for each problem
       for problem in acoustic tti elastic viscoelastic; do

           if [[ $problem == tti ]]; then
               if [[ $compiler == icc ]]; then
                   bms="O2 O3"
               else
                   bms="O2"
               fi
           else
               bms="O3"
           fi

           # for each devito optimization mode
           for bm in $bms; do

               # for each grid size
               for grid in 512; do

                   # for each space order
                   for so in `seq 4 4 16`; do

                       dir=results-devito-benchmark/$machine/$compiler/$be/$problem/$bm/grid$grid

                       logdir=$dir/so$so
                       mkdir -p $logdir

                       log=$logdir/run.log
                       rm -f $log
                       touch $log
                       date >> $log
                       uname -a >> $log
                       lscpu >> $log
                       icpc -v 2>&1 >> $log
                       uname -a >> $log
                       numactl -H >> $log
                       numastat -m >> $log
                       top -b -n 1 | head -30 >> $log

                       cmd="env OMP_NUM_THREADS=$cores $plat DEVITO_LOGGING=DEBUG DEVITO_DEBUG_COMPILER=1 DEVITO_AUTOTUNING=aggressive DEVITO_BACKEND=$be DEVITO_OPENMP=1 DEVITO_ARCH=$compiler $nc python3 devito/benchmarks/user/benchmark.py bench -bm $bm -P $problem -so $so -to 2 -d $grid $grid $grid --tn 1000 -x $nruns -r $dir"

                       echo $cmd >> $log
                       $cmd |& tee -a $log
                       date >> $log

                   done
               done
           done
       done
   done
done
